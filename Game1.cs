﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace target_game
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Texture2D target_Sprite { get; private set; }
        public Texture2D crosshairs_Sprite { get; private set; }
        public Texture2D background_Sprite { get; private set; }
        public SpriteFont gameFont { get; private set; }
        public float timer = 10f;
        public int score = 0;
        const int TARGET_RADIUS = 45;
        const int CROSSHAIRS_RADIUS = 25;

        float mouseTargetDist;
        bool mReleased = true;

        Vector2 targetPosition = new Vector2(300,300);

        MouseState mState;

        public Vector2 mPosition { get; private set; }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteBatch = new SpriteBatch(GraphicsDevice);

            target_Sprite = Content.Load<Texture2D>("target");
            crosshairs_Sprite = Content.Load<Texture2D>("crosshairs");
            background_Sprite = Content.Load<Texture2D>("sky");

            gameFont = Content.Load<SpriteFont>("galleryFont");
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            mState = Mouse.GetState();

            if (timer > 0)
            {
                timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                mouseTargetDist = Vector2.Distance(targetPosition, new Vector2(mState.X, mState.Y));
                if (mState.LeftButton == ButtonState.Released)
                {
                    mReleased = true;
                }

                if (mState.LeftButton == ButtonState.Pressed && mouseTargetDist < TARGET_RADIUS && mReleased == true)
                {
                    score++;
                    Random r = new Random();
                    targetPosition.X = r.Next(TARGET_RADIUS, graphics.PreferredBackBufferWidth - TARGET_RADIUS);
                    targetPosition.Y = r.Next(TARGET_RADIUS, graphics.PreferredBackBufferHeight - TARGET_RADIUS);
                    mReleased = false;
                }

            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            spriteBatch.Draw(background_Sprite, new Vector2(0, 0), Color.White);


            if (timer > 0)
            {
                spriteBatch.Draw(target_Sprite, new Vector2(targetPosition.X - TARGET_RADIUS, targetPosition.Y - TARGET_RADIUS), Color.White);
            }
            spriteBatch.Draw(crosshairs_Sprite, new Vector2(mState.X - CROSSHAIRS_RADIUS, mState.Y - CROSSHAIRS_RADIUS), Color.White);

            spriteBatch.DrawString(gameFont, "Score: " + score.ToString(), new Vector2(3, 3), Color.White);
            spriteBatch.DrawString(gameFont, "Time: " + Math.Ceiling(timer).ToString(), new Vector2(3, 40), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
